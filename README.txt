
Description
-------------
Profile Map enables arbitrary profile fields to be mapped to a fixed data model
in the $user object so that third-party modules can easily access the profile 
data. The module provides callbacks when the mapped profile fields are changed,
for example, to require a form to be resubmitted on an address change. Profile 
Map also optionally themes the mapped profile fields to display as a single 
fields on the user page, such as Full Name instead of two separate fields for 
First and Last names.

Profile Map categories:

* Full Name: first, last
* Address: street, city, state, postal, country
* Phone: country code, area code, number
* Messaging: service, username


Dependencies
-------------
Profile module


Installation
-------------
1. install module: copy profile_map directory and all its contents to your 
   modules directory
2. enable module: admin/build/modules
3. configure module: admin/settings/profile_module


Download
-------------
Download package and report bugs, feature requests, or submit a patch from the 
project page on the Drupal web site.
http://drupal.org/project/profile_map


Developers
-------------
Profile Map adds the following data model to the $user object:

stdClass Object (
  [profile_map] => Array (
    [name] => Array (
      [first] => Theodore
      [last] => Williams
      [view] => Theodore Williams
    )
    [address] => Array (
      [street] => 4 Yawkey Way
      [city] => Boston
      [state] => MA
      [postal] => 02215
      [country] => United States
      [view] => 4 Yawkey Way, Boston, MA 02215
    )
    [phone] => Array (
      [countrycode] => 
      [areacode] => 999
      [number] => 999-9999
      [view] => (999) 999-9999
    )
    [messaging] => Array (
      [service] => AIM
      [username] => 400clubmember
      [view] => AIM: 400clubmember
    )
  )
)


Todo List
-------------
None


Author
-------------
John Money
ossemble LLC.
http://ossemble.com

Module development sponsored by ConsumerSearch, a service of About.com, a part
of The New York Times Company.
http://www.consumersearch.com
